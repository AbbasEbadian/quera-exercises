from sys import stdin
n, q = input().split()
k = [0]*len(q)
mx = int(q, 2)
s2 = q
xx = [[int(x) for x in x.strip().split()] for x in stdin.readlines()]
xx.sort(key=lambda x: x[1]-x[0])
for a,b in xx:
    if all([k[x] for x in range(a-1,b)]):
        continue
    
    s = s2[:a-1] + s2[a-1:b][::-1] + s2[b:]
    mxi = max(mx, int(s, 2))
    print(a,b, s)
    if mxi != mx:
        mx = mxi
        k[a-1:b] = [1]*(b-a)
        q = s
        s2 = s
    print(k)

print(bin(mx)[2:].zfill(len(q)))
