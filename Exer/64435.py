from sys import stdin
a = int(input())
for i in range(a):
    n,m= [int(x) for x in input().split()]
    t = []
    for z in range(n):
        t.append(input())
    s = 0
    for j in range(m):
        for i in range(n):
            if t[i][j] == '.': continue
            if t[i][j] == '_':
                s+= n - (i+1)
                break
            elif t[i][j] == "/" or t[i][j] == "\\" :
                s+= n - (i+1) + 0.5
                break
    print("{:0.3f}".format(s))
                
