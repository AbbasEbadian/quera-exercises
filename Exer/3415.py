import sys
k =  int(input())
f = []
for s in sys.stdin.readlines():
    f.append([int(x) for x in s.strip().split()])

c = 0
for i in f:
    for j in f:
        if i==j: continue
        if i[1] <= j[1] and i[0] >= j[0]:
            c+=1
            break
    
print(k-c)
