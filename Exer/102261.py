n= int(input())

for _ in range(n):
    c = 0
    x,y = list(map(int, input().split()))
    x = int(x**0.5) if int(x**0.5) == x**0.5 else int(x**0.5)+1
    y = int(y**0.5)
    print(y-x+1)
