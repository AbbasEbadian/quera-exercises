from itertools import dropwhile
from math import log
input()
k =  input()
s,t= [int(x)-1 for x in input().split()]
k = list(k[min(s, t): max(s,t)])
f = 0
while len(k):
    s = list(dropwhile(lambda x: x=="P", k))
    a = len(k) - len(s)
    k = s
    b = list(dropwhile(lambda x: x=="H", k))
    c = len(k) - len(b)
    while c > 0:
        v = 0
        n = 0
        while v < c:
            v += 2 ** n
            n += 1
        c -= 2 ** (n-1)
        f += 1
    k = b
print(f)
