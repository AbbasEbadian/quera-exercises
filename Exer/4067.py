t = {(0,0):0}
n = int(input())
s = 0
for i in range(n):
    a,b = [int(x) for x in input().split()]
    
    x,y = list(t.keys())[-1]
    while (x!= a or  y !=b) and (x <= a or y <= b):
        if   (s+1)%4==1: x+=1;y+=1;
        elif (s+1)%4==2: x+=1;y-=1;
        elif (s+1)%4==3: x+=1;y+=1;
        elif (s+1)%4==0: x-=1;y+=1;
        s+=1 
        t[(x,y)] = s
    t[(x,y)] = s
    if (a,b) in t.keys():
        print(t[(a,b)])
    else:
        print(-1)

        
