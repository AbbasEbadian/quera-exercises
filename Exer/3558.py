n, m = [int(x) for x in input().split()]
a,b = [],[]
for i in range(n):
    l,r = [int(x) for x in input().split()]
    a.extend(list(range(l,r+1)))


for i in range(m):
    l,r = [int(x) for x in input().split()]
    b.extend(list(range(l,r+1)))

print(len(set(a).intersection(set(b))))
        
        
    
