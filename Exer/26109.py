p1, s1 = [int(x) for x in input().split()]
s2, p2 = [int(x) for x in input().split()]

if p1+p2 > s1+s2:
    print("Persepolis")
elif s1+s2 > p1+p2:
    print("Esteghlal")
else:
    if p2 > s1:
        print("Persepolis")
    elif s1 > p2:
        print("Esteghlal")
    else:
        print("Penalty")
