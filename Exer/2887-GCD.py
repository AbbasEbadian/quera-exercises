n= int(input())
import functools
import math
a = [int(x) for x in input().split()] 
gcd = functools.reduce(math.gcd,a )
print(sum([x//gcd for x in a]))
