def checkPattern(string, pattern):
    j = 0
    for i in range(len(string)):
        if string[i] == pattern[j]:
            j+=1
            if j == len(pattern):
                break
    return j == len(pattern)
n= int(input()) 
for i in range(n):
    string = input()
    pattern = input()
   
    x = checkPattern(string, pattern)
    if not x:
        x = checkPattern(string[::-1], pattern)
    print(x and "YES" or "NO")
 
