#Kashi
import sys
n, m = [int(x) for x in input().split()]
A = [0]+[int(x.strip()) for x in sys.stdin.readlines()]
inf = 10**10 - 1
dp = [[-1]*(m+1) for _ in range(n+1)]
def mem(pos, need):
    if pos == 0:
        if need == 0:
            return 0
        return inf
    ret = dp[pos][need]
    if ret != -1:
        return ret
    ret = inf
    k = 0
    while k*k<=need:
        a =  mem(pos-1, need-k*k)
        b = (A[pos]-k)**2
        ret = min(ret,a+b)
        
        k+=1
    dp[pos][need] = ret
    return ret

ans = min(inf, mem(n,m))
print(*dp, sep="\n")
print(ans!=inf and ans or -1)
