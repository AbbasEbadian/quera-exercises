from math import gcd
n = int(input())
#n, dd = [int(x) for x in input().split()]
a = [int(input())]
lcm = a[0]
for _ in range(n-1):
    i = int(input()) 
    lcm = lcm*i//gcd(lcm, i)
print(lcm%30 + 1)
    
