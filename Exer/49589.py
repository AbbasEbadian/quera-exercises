#from itertools import dropwhile as dw
n, m = [int(x) for x in input().split()]
import sys
k = [x.strip() for x in sys.stdin.readlines()]
c = 0    
v = 4
while v <= n:
    for i in range(n-v+1):
        for j in range(m-v//2+1):
            if k[i][j] !="*": continue
            a = "".join([k[x][j] for x in range(i, i+v)])
            b = "".join([k[i+v-1][x] for x in range(j, j+v//2)])
            if a == "*"*v and b == "*"*(v//2):
                c+=1
    v+=2
                
print(c)
