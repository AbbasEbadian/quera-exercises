a,b = [int(x) for x in input().split()]
t = ""
for i in range(a):
    t += "X"*b + "."*b + "X"*b + "\n"
for i in range(a):
    t += "."*b + "X"*b + "."*b + "\n"
for i in range(a):
    t += "X"*b + "."*b + "X"*b
    if i+1<a:
        t+= "\n"
print(t)
