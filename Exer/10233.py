#n, m = [int(x) for x in input().split()]
a = int(input())
k = sorted(list(str(a)))
for i in range(a+1, int("1"+len(str(a))*"0")):
    if k == sorted(list(str(i))):
        print(i)
        exit()
print(0)
