#Kashi
import sys
from array import array
n= int(input())
a = [int(x) for x in input().split()]
b = [int(x) for x in input().split()]
c = [int(x) for x in input().split()]

m = 0
i = 0
dp = {}
ii = 0
while i<len(a):
    aac = a.count(a[i])
    bac = b.count(a[i])
    cac = c.count(a[i])
    abc = a.count(b[i])
    bbc = b.count(b[i])
    cbc = c.count(b[i])
    acc = a.count(c[i])
    bcc = b.count(c[i])
    ccc = c.count(c[i])
    if not all([aac, bac, cac,  abc, bbc, cbc,  acc, bcc, ccc]):
        m+=1
        del a[i];del b[i];del c[i];
        continue
    
    aap = aac > bac or aac > cac 
    bap = bac > aac or bac > cac
    cap = cac > aac or cac > bac
    
    abp = abc > bbc or abc > cbc 
    bbp = bbc > abc or bbc > cbc
    cbp = cbc > abc or cbc > bbc
    
    acp = acc > bcc or acc > ccc 
    bcp = bcc > acc or bcc > ccc
    ccp = ccc > acc or ccc > bcc
    cond = (aap or bap or cap or abp or bbp or cbp or acp or bcp or ccp) and len(set([a[i], b[i], c[i]])) == 3
    if cond:
        del a[i];del b[i]; del c[i]
        m+=1
        continue
    else:
        i+=1
print(m)
