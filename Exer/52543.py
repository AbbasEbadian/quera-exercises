k =  int(input())
x = [int(x) for x in input().split()]
f = []
m = True
while x:
    if m:
        a = x.index(max(x))
        f.append(x.pop(a))
    else:
        a = x.index(min(x))
        f.append(x.pop(a))
    m = not m
print(*f)
