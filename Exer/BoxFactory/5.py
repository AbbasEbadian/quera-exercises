class Driver:
        id_counter = 1
        def __init__(self, un, ser_cat, pos):
                self.id = Driver.id_counter
                self.username = un
                self.service_category = ser_cat
                self.credit = 0
                self.position = pos
                self.status = "FREE"
                self.orders = []

                Driver.id_counter += 1
                
        def __repr__(self):
                return f"{self.status} {self.position} {int(self.credit)}"

class Order:
        id_counter = 1
        def __init__(self, ser_cat, s_pos, f_pos, pending_orders=0):
                self.id = Order.id_counter
                self.service_category = ser_cat
                self.starting_position = s_pos
                self.finishing_position = f_pos
                self.cost = Order.compute_cost(s_pos, f_pos, pending_orders+1)
                self.status = "PENDING"
                self.driver = None

                Order.id_counter += 1

        def __repr__(self):
                return str(self.id)


        @staticmethod
        def compute_cost(start, finish, pending_orders):
                dist = pending_orders + Server.calc_dist(start, finish) 
                return dist * 100

class Server():
        
        def __init__(self):
                self.drivers = []
                self.orders = []
                self.company_profit = 0

        @staticmethod
        def calc_dist(a,b):
                        return abs(a[0]-b[0]) + abs(a[1]-b[1])

        def company(self, x):
                print(int(self.company_profit))
                
        def add_driver(self, cmd):
                cmd = cmd.replace("ADD-DRIVER", "").strip()
                username = cmd.split()[0]
                ser_cat = cmd.split()[-1]
                pos = tuple([int(x.strip()) for x in cmd[cmd.index("(")+1:cmd.index(")")].split(',')])
                driver = filter(lambda x: x.username == username, self.drivers)
                if len(list(driver)):
                        print("user previously added")
                else:
                        self.drivers.append(Driver(username, ser_cat, pos))
                        print("user added successfully")

        def get_driver(self, cmd):
                username = cmd.split()[-1].strip()
                driver = list(filter(lambda x: x.username == username, self.drivers))
                if len(driver):
                        print(driver[0])
                else:
                        print("invalid driver name")
                        
        def get_drivers(self, cmd):
                status = cmd.split()[-1].strip()
                drivers = list(filter(lambda x: x.status == status, self.drivers))
                if len(drivers):
                        print(*[d.username for d in drivers], sep=" ")
                else:
                        print("NONE")

        def get_near_driver(self, cmd):
                cmd = cmd.replace("GET-NEAR-DRIVER", "").strip()
                count = int(cmd.split()[-1].strip())
                pos = tuple([int(x.strip()) for x in cmd[cmd.index("(")+1:cmd.index(")")].split(',')])
                
                drivers = sorted(self.drivers, key=lambda x:Server.calc_dist(x.position, pos))
                drivers = [d for d in drivers if   d.status=="FREE"]
                c = 0
                # almost a bubble sort
                while c < count+1 and c <len(drivers)-1:
                        if Server.calc_dist(drivers[c].position, pos) ==Server.calc_dist(drivers[c+1].position, pos):
                                if drivers[c].id > drivers[c+1].id :
                                        drivers[c], drivers[c+1] = drivers[c+1], drivers[c]
                        c+=1
               
                if drivers:
                        print(*[d.username for d in drivers[:count]])
                else:
                        print("None")

        def create_order(self, cmd):
                cmd = cmd.replace("CREATE-ORDER", "").strip()
                category = cmd.split()[0].strip()
                s_pos = tuple([int(x.strip()) for x in cmd[cmd.index("(")+1:cmd.index(")")].split(',')])
                cmd = cmd[cmd.index(")")+1: ]
                f_pos = tuple([int(x.strip()) for x in cmd[cmd.index("(")+1:cmd.index(")")].split(',')])
                if s_pos == f_pos:
                        print("invalid order")
                else:
                        pending_orders = [x for x in self.orders if x.status=="PENDING" and x.service_category == category]
                        order = Order(category, s_pos, f_pos, len(pending_orders))
                        self.orders.append(order)
                        print(order)
                        
        def get_order(self, cmd):
                order_id = cmd.split()[-1].strip()
                order = list(filter(lambda x: x.id == int(order_id), self.orders))
                if len(order):
                        if order[0].driver:
                                print(order[0].status, order[0].driver.username, order[0].cost)
                        else:
                                print(order[0].status, None, order[0].cost)

                        
                else:
                        print("invalid order")

        def get_orders(self, cmd):
                status = cmd.split()[-1].strip()
    
                #print([(order.id, order.status, order.cost, order.driver) for order in self.orders])
                orders = list(filter(lambda x: x.status == status, self.orders))
                if orders:
                        print(*[d.id for d in orders], sep=" ")
                else:
                        print("None")

        def order_update(self, cmd):
                cmd = cmd.replace("ORDER-UPDATE", "").strip()
                status, username, order_id  = [x.strip() for x in cmd.split() if x]
                driver = list(filter(lambda x: x.username == username, self.drivers))
                if not len(driver):
                        print("invalid driver name")
                        return
                
                # Not mentioned in question, if order does not exist
                order = list(filter(lambda x: x.id == int(order_id), self.orders))
                if not len(order):
                        print("invalid order")
                        return
                
                driver = driver[0]
                order = order[0]
                if not order.driver or order.driver.id != driver.id:
                        print("wrong order-id")
                        return
                statuses = ["PENDING", "ARRIVED", "PICKUP", "DELIVERED"]
                if statuses.index(status) == 0 or  statuses[statuses.index(status)-1] != order.status :
                        print("invalid status")
                        return
                order.status = status
                if status == "DELIVERED":
                        order.driver.credit += order.cost * 0.8
                        self.company_profit += order.cost * 0.2
                        order.driver.status = "FREE"
                        order.driver.position = order.finishing_position
                elif status == "PICKUP":
                        driver.position = order.starting_position
                
                print("status changed successfully")

        def get_cnt_order(self, cmd):
                cmd = cmd.replace("ORDER-UPDATE", "").strip()
                pos = tuple([int(x.strip()) for x in cmd[cmd.index("(")+1:cmd.index(")")].split(',')])
                cmd = cmd[cmd.index(")")+1: ]
                distance, target = [x.strip() for x in cmd.split()]
                distance = int(distance)
                orders = []
                if target == "START":
                        orders = [x for x in self.orders if Server.calc_dist(x.starting_position, pos) <= distance]
                else:
                        orders = [x for x in self.orders if Server.calc_dist(x.finishing_position, pos) <= distance ]
                
                print(len(orders))
                
        def assign_next_order(self, cmd):
                username = cmd.split()[-1].strip()
                driver = list(filter(lambda x: x.username == username, self.drivers))
                if not len(driver):
                        print("invalid driver name")
                        return
                driver = driver[0]
                if driver.status == "BUSY":
                        print("driver is already busy")
                        return
                orders = [x for x in self.orders if x.status=="PENDING" and x.service_category == driver.service_category]
                if not len(orders):
                        print("there is no order right now")
                        return
                c = 0
                sorted_orders = sorted(orders, key=lambda x:Server.calc_dist(x.starting_position, driver.position))
                while c <len(sorted_orders)-1:
                        if Server.calc_dist(sorted_orders[c].starting_position, driver.position) == Server.calc_dist(sorted_orders[c+1].starting_position, driver.position):
                                if sorted_orders[c].id > sorted_orders[c+1].id :
                                        sorted_orders[c], sorted_orders[c+1] = sorted_orders[c+1], sorted_orders[c]
                        c+=1
                next_order = sorted_orders[0]
                next_order.status = "ARRIVED"
                next_order.driver = driver
                driver.status = "BUSY"
                #print([(order.id, order.status, order.cost, order.driver) for order in self.orders])
                print(next_order.id, "assigned to", driver.username)
        def nearest_pending_order(self, cmd):
                pos = tuple([int(x.strip()) for x in cmd[cmd.index("(")+1:cmd.index(")")].split(',')])
                orders = sorted([x for x in self.orders if x.status == "PENDING"], key=lambda x: Server.calc_dist(x.starting_position, pos))
                if orders :
                        print(orders[0])
                else:
                        print("None")
                
                
                        
server = Server()
import sys
commands = {
        "ADD-DRIVER": server.add_driver,
        "GET-DRIVER": server.get_driver,
        "GET-DRIVER-LIST": server.get_drivers,
        "GET-NEAR-DRIVER": server.get_near_driver,
        "CREATE-ORDER": server.create_order,
        "GET-ORDER": server.get_order,
        "GET-ORDER-LIST":server.get_orders,
        "ORDER-UPDATE": server.order_update,
        "ASSIGN-NEXT-ORDER": server.assign_next_order,
        "GET-CNT-ORDER": server.get_cnt_order,
        "GET-COMPANY": server.company,
        "GET-NEAREST-PENDING-ORDER": server.nearest_pending_order,
        "END": exit
        
        
}
line = input()
while line != "END":
        key = line.split()[0]
        commands[key](line)
        line =input()
#for line in sys.stdin:
#        line = line.strip()
#        key = line.split()[0]
#        commands[key](line)
