# This question can be implemented by Tokenizing and AST.
# For sake of simplicity we stick with 'if else'.
from sys import stdin

def _split(exp, sep=" "):
        return [x.strip() for x in exp.split(sep) if x]

def _check(condition):
        if not condition: return True;
        a,b = _split(condition, '==')
        try: a = var_dict[a]
        except: a = int(a);
        try: b = var_dict[b]
        except: b = int(b);
        return a==b

def handle_summation(exp):
        a,b = _split(exp, '+')
        try: a = var_dict[a]
        except: a = int(a)
        try: b = var_dict[b]
        except: b = int(b)
        return a+b;
        
def handle_substraction(exp):
        a,b = _split(exp, '-')
        try: a = var_dict[a]
        except: a = int(a)
        try: b = var_dict[b]
        except: b = int(b)
        return a-b

def handle_assignment(exp):
        global input_counter
        left, right = _split(exp, '=')
        if right.find('+')>-1: var_dict[left] = handle_summation(right)
        elif right.find('-')>-1: var_dict[left] = handle_substraction(right)
        elif right.find("voroodi")>-1:
                var_dict[left]=int(input_vars[input_counter])
                input_counter += 1
        else: var_dict[left] = int(right)
        
def handle_output(exp):
        global output
        exp = exp.replace("khoorooji(", "").replace(")", "")
        lst = _split(exp, ",")
        out = []
        for item in lst:
                try: out .append(str(var_dict[item]))
                except: out.append(str(item))
        output.append(" ".join(out))

def handle_conditioned_assignment(exp):
        condition, exp2 = _split(exp, ":")
        condition= condition.replace("agar", "").strip()
        if not _check(condition): return
        handle_assignment(exp2)

var_dict = {} # To store variables
output = []
input_counter = 0
input_vars = []
lines = []
has_seen_end = False
for line in stdin:
        if line.find("-----")>-1:
                has_seen_end = True
                continue
        if has_seen_end:
               input_vars.append(line.strip())
        else:
                lines.append(line.strip())
for exp in lines:
        if exp.find("agar")>-1: handle_conditioned_assignment(exp)
        elif exp.find("=")>-1: handle_assignment(exp)
        elif exp.find("khoorooji")>-1: handle_output(exp)
        
print(*output, sep="\n")
print(len(var_dict))
