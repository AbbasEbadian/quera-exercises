from sys import stdin

# Fastest method to findout if given n is prime or not. :)
def is_prime(n):
        if n <2: return False
        if n <4: return True;
        if n%2 == 0 or n%3 == 0: return False
        r = 5
        while r < n**0.5:
                if n%r==0 or n%(r+2) == 0: return False
                r += 6
        return True

def inp():
        return list(map(int, stdin.readline().strip().split()))

grid = []
n,m,k = inp()
for _ in range(n): grid.append(inp());
x,y = 0, 0
while k> 0:
        k-=1
        v = grid[x][y]
        if is_prime(v):
                x = n-x-1; 
                y = m-y-1
        else:
                if v%4 == 0: y = (y+1)%m
                elif v%4==2: y = y-1 if y>0 else m-1
                elif v%4==1: x = (x+1)%n
                elif v%4==3: x = x-1 if x>0 else n-1
                
print(x+1, y+1)
