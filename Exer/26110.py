n = int(input())
s = 0
for _ in range(n):
    
    x,y = [int(x) for x in input().split()]
    d = (x**2 + y**2) ** 0.5
    if d <=10: s+=10
    elif 10<d<=30: s+=9
    elif 30<d<=50: s+=8
    elif 50<d<=70: s+=7
    elif 70<d<=90: s+=6
    elif 90<d<=110: s+=5
    elif 110<d<=130: s+=4
    elif 130<d<=150: s+=3
    elif 150<d<=170: s+=2
    elif 170<d<=190: s+=1
    else: pass

print(s)

