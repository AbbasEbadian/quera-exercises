import sys
n,m,k = list(map(int, input().split()))
if k%2 == 1:
    print("0")
    exit()
s = [[True]*m for _ in range(n)]

for i in sys.stdin.readlines():
    x,y= list(map(int, i.strip().split()))
    s[x-1][y-1] = False
if not sum([sum(x) for x in s]):
    print("-1")
    exit()
print(1)
for i in range(n):
    for j in range(m):
        if s[i][j]:
            print(i+1, j+1)
            exit()
        



