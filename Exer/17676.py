
from sys import stdin
n,m= list(map(int, input().split()))
s = []
for i in range(n):
    s.append(list(input()))

def explore(i, j):
    global s
    k = s[i][j]
    
    s[i][j] = "*"
    if k == "|":
        for v in range(j+1, m):
            if s[i][v] == "|":
                s[i][v] = "*"
    
    elif k == "-":
        for v in range(i+1, n):
            if s[v][j] == "-":
                s[v][j] = "*"


c = 0
for i in range(n):
    for j in range(m):
        if s[i][j] == "*":
            continue
        explore(i,j)
        c +=1
        
print(c)
