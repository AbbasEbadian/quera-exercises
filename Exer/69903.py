m = [31,31,31,31,31,31,30,30,30,30,30,29]
m1, d1 = [int(x) for x in input().split()]
m2, d2 = [int(x) for x in input().split()]
_m1 = sum(m[:m1-1]) + d1
_m2 = sum(m[:m2-1]) + d2
print(abs(_m1 - _m2))
