n,m = list(map(int, input().split()))
s ="1"
while len(s)<= m:
    s += "".join(["0" if x=="1" else "1" for x in s ])
print(s[n-1:m])
