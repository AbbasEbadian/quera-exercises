a, b = list(map(int, input().split()))
k = list(range(1, a+1))
f = [k.pop(0)]
while 1:
    d = [x-f[-1]>=b and x-f[-1] or 9999999999 for x in k  ]
    if not d or all([x==9999999999 for x in d]):
        print("Impossible")
        exit()
    c = d.index(min(d))
    f.append(k.pop(c))
    if len(f) == a:
        break

print(*f)
    
    
        
