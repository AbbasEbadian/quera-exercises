k = [input(), input(), input()]
b = ""
for i in range(len(k[0])//5):
    f = [k[j][i*5:i*5+5] for j in range(3)]
    letter = "X"
    if f[0] == "*"*5:
        letter = "T"
    elif f[1][1:-1] == "***":
        letter = "A"
    elif f[0][0] == f[0][-1] == f[1][0] == f[1][-1] == f[2][0] == f[2][-1] == '*':
        if f[0][1] == "*":
            letter = "M"
        else:
            letter = "N"
    
    b += letter

print(b)
