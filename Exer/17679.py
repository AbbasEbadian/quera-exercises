from array import array
from sys import stdin

n, q = list(map(int, input().split()))
arr_idx = array("L", [0]*(n+1))
arr_sum = array("L", [])
x = [x.strip().split() for x in stdin.readlines()]
for v in x:
    if v[0] == '1':
        if not arr_sum:
            arr_sum.append(int(v[1]))
        else:
            arr_sum.append(int(v[1])  + arr_sum[-1])
    else:
        i,j = int(v[1]), int(v[2])
        if j == 0:
            print(0)
            continue
        f = arr_idx[i]
        if f > 0:
            print(arr_sum[f+j-1] - arr_sum[f-1])
        else:
            print(arr_sum[j-1])
        arr_idx[i] += j
        
