k =  int(input())
import math
#n,m = [int(x) for x in input().split()]
if math.log(k, 2) == int(math.log(k, 2)):
    print("Yes")
    exit()
a = []
while k >1 :
    if k%2 == 0:
        k //= 2
    else:
        k = k*3+3
    if k in a:
        print("No")
        exit()
    a.append(int(k))
if k == 1:
    print("Yes")
else:
    print("No")
    
