#n,k= [int(x) for x in input().split()]
a = input()
b = input()
c = 0
for i in range(len(a)-len(b)+1):
    t = list(b)
    k = True
    for j in range(i, i+len(b)):
        if a[j] == "?":continue
        if a[j] not in t:
            k = False
            break
        t[t.index(a[j])] = "-"
    if k: c +=1

print(c)
