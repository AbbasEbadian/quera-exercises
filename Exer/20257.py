k, a, b = [int(x) for x in input().split()]
s,v = min(a,b), max(a,b)
dt = v-s


s1 = s%k
sd = -1
if k-s%k <= s1:
    s1 = k-s%k
    sd = 1
v1 = v%k
vd = -1
if k-v%k < v1:
    v1 = k-v%k
    vd = 1
t = s1 + v1
p1 = s + s1*sd
p2 = v + v1*vd
t += (p2-p1)//k


print(min(t, dt))
