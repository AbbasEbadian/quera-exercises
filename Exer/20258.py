n,m= list(map(int, input().split()))
from sys import stdin
k = sorted([[int(a) for a in x.strip().split()] for x in stdin.readlines()], key=lambda x: (x[0], x[1]))
last_time = k[0][0]
last_r = k[0][1]
last_time_c = 1
last_r_c = 1
for i in range(1, n):
    if k[i][0] == last_time:
        if k[i][1] == last_r:
            last_r_c += 1
            if last_r_c > m:
                print("NO")
                exit()
        else:
            last_r = k[i][1]
            last_r_c = 1
    else:
        last_time = k[i][0]
        last_r = k[i][1]
        last_r_c = 1

print("YES")
    
