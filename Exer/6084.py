n = int(input())
k = [int(x) for x in input().split()]
for i in range(1,  n+1):
    if n%i != 0: continue
    s = sum(k[:i])
    f= True
    for j in range(i, n, i):
        if sum(k[j:j+i]) != s:
            f = False
            break
    if f:
        print(n//i)
        exit()
        
    
