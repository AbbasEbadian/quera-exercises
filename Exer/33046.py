
nodes = int(input())
g = [0] * (nodes+1)

for i in range(nodes-1):
    a,b = list(map(int, input().split()))
    g[a] += 1
    g[b] += 1
print(max(g))      
