#k =  int(input())
x = [int(x) for x in input().split()]
a = sum(x)/3
print(max(sum([a!=f for f in x]) -1, 0))
