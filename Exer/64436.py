from sys import stdin
n,m= [int(x) for x in input().split()]
t = [x.strip() for x in stdin.readlines()]
t = [[x=="*" for x in r] for r in t]
def nals(i,j):
    c = 0

    if t[i][j] and  t[i+1][j] and t[i+2][j]  and t[i+2][j+1] and t[i+2][j+2] and t[i+1][j+2]: c+=1
    if t[i][j+2] and  t[i+1][j] and t[i+2][j]  and t[i+2][j+1] and  t[i+2][j+2] and t[i+1][j+2]: c+=1
    if t[i][j] and  t[i+1][j] and t[i+2][j+2]  and t[i][j+1] and t[i][j+2] and t[i+1][j+2]: c+=1
    if t[i][j] and  t[i+1][j] and t[i+2][j]  and t[i][j+1] and t[i][j+2] and t[i+1][j+2]: c+=1
    
    if t[i][j] and t[i][j+1] and t[i][j+2] and  t[i+1][j+2] and t[i+2][j+2]   and t[i+2][j+1]: c+=1
    if t[i][j+1] and t[i][j+2] and  t[i+1][j+2] and t[i+2][j] and t[i+2][j+2]   and t[i+2][j+1]: c+=1
    if t[i][j+1] and t[i][j] and  t[i+1][j] and t[i+2][j] and t[i+2][j+1] and t[i+2][j+2]: c+=1
    if t[i][j+1] and t[i][j] and  t[i+1][j] and t[i+2][j] and t[i+2][j+1] and t[i][j+2]: c+=1
    return c
c = 0
for i in range(n-2):
    for j in range(m-2):
        c += nals(i,j)
print(c)
