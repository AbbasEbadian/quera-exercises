from sys import stdin
n = int(input())
for _ in range(n):
    c, k= [int(x) for x in input().split()]
    t = input().split()
    res = ""
    new = ""
    end = "|"
    endl = True
    for i in range(len(t)):
        cw = endl and t[i].capitalize() or t[i].lower()
        cw = cw.strip()
        if len(new) +len(cw) + (new!='' and 1 or 0) <=k:
            new = new + (new!='' and " " or '') + cw
        else:
            if new.find(" ")>-1:
                
                mew = new.split()
                spaces = new.count(" ")
                sp = [(k-len(new)+spaces)//spaces]*spaces
                
                rem = k-len(new) - sum(sp) + spaces
                neg = -1
                u = 1
                while rem>0:
                    if neg == 1:
                        sp[u-1]+=1
                        u+=1
                    else:
                        sp[neg*u]+=1
                    neg *= -1
                    
                    
                    rem -= 1
                mm = ""
                for j in range(len(mew)):
                    mm += mew[j]
                    if j < len(sp):
                        mm += sp[j]*" "        
                new = mm
            else:
                new +=  " "*(k-len(new))
            res += "|"+new+"|\n"
            
            new = cw
            if i == len(t)-1:
                new +=  " "*(k-len(new))
                res += end+new+end
        endl =  t[i][-1] in [".", "!", "?"]
            
    print(res)
            

