n,m,k= [int(x) for x in input().split()]
s = [[[0,0]]*m for _ in range(n)]
for i in range(n):
    a,b = [int(x) for x in input().split()]
    s[a-1][b-1][1] += 1

for i in range(k):
    a,b = [int(x) for x in input().split()]
    s[a][b][0] += 1
print(*s, sep="\n")
