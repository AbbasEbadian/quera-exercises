#n, m = [int(x) for x in input().split()]
import sys
n,m = 7, 7
a = [x.strip() for x in sys.stdin.readlines()]
c = 0
for x in range(n):
    for y in range(m):
        if a[x][y] == 'o':
            if x>1 and a[x-1][y]=="o" and a[x-2][y]==".":
                c+=1

            if x<n-2 and a[x+1][y]=="o" and a[x+2][y]==".":
                c +=1
            if y>1 and a[x][y-1]=="o" and a[x][y-2]==".":
                c +=1
            if y<m-2 and a[x][y+1]=="o" and a[x][y+2]==".":
                c +=1
print(c)
