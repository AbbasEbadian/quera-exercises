
def rotate(mat):
 
    # base case
    if not mat or not len(mat):
        return
 
    # `N × N` matrix
    N = len(mat)
 
    # Transpose the matrix
    for i in range(N):
        for j in range(i):
            temp = mat[i][j]
            mat[i][j] = mat[j][i]
            mat[j][i] = temp
 
    # swap columns
    for i in range(N):
        for j in range(N // 2):
            temp = mat[i][j]
            mat[i][j] = mat[i][N - j - 1]
            mat[i][N - j - 1] = temp
 
n= int(input())
x = []
for i in range(n):
    x.append(list(input()))
k = int(input())

for i in range(k):
    c = input()
    if c=="V":
        for i in range(n):
            for j in range(n//2):
                x[i][j], x[i][n-1-j] = x[i][n-j-1], x[i][j]
    elif c=="H":
        for j in range(n):
            for i in range(n//2):
                x[i][j], x[n-i-1][j] = x[n-i-1][j], x[i][j]
    else:
        rotate(x)
for r in x:
    print("".join(r))
