n,m = [int(x) for x in input().split()]
d = {}
for i in range(n):
    s,v = input().split()
    d[s] = v

k = [(x in d and d[x]+" " or "") + "kachal!"  for x in input().split()]
print(*k)
