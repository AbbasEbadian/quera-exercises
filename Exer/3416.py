from itertools import dropwhile as dw
n = int(input())
for i in range(n):
    s = ""
    if  input() ==  '1':
        k = list(input())
        while len(k):
            v = k[0]
            kk = list(dw(lambda x: x==v, k))
            s += v
            if len(k) - len(kk) > 1:
                 s += str(len(k) - len(kk))
            k = kk

    else:
        j = 0
        k = list(input())
        while j < len(k):
            if k[j].isdigit() and int (k[j]) >1:
                s += k[j-1] * (int(k[j])-1)
            else:
                s += k[j]
            j += 1
    print(s)
