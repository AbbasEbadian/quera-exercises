n, m = [int(x) for x in input().split()]
k = int(input())
a = [[0]*m for _ in range(n)]
for _ in range(k):
    x,y = [int(x)-1 for x in input().split()]
    a[x][y] = "*"
    if x>0 and a[x-1][y]!="*": a[x-1][y]+=1 # Top
    if x<n-1 and a[x+1][y]!="*": a[x+1][y]+=1 # Bottom
    if y>0 and a[x][y-1]!="*": a[x][y-1]+=1 # Left
    if y<m-1 and a[x][y+1]!="*": a[x][y+1]+=1 #Right
    if x>0 and y>0 and a[x-1][y-1]!="*": a[x-1][y-1]+=1 # Top-Left
    if x<n-1 and y>0 and a[x+1][y-1]!="*": a[x+1][y-1]+=1 # Bottom-Left
    if x>0 and y<m-1 and a[x-1][y+1]!="*": a[x-1][y+1]+=1 # Top-Right
    if x<n-1 and y<m-1 and a[x+1][y+1]!="*": a[x+1][y+1]+=1 # Bottom-Right
    
for r in a:
    print(*r)
