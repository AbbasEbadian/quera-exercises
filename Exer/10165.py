from sys import stdin
n = int(input())
a = [list(map(int, x.strip().split())) for x in stdin.readlines()]

i, j= 0, n-1
max_ald = 0
min_dis = 10**10-1
while i!=j:
    pm = max_ald
    max_ald = max(len(set([a[z][1] for z in range(n)])), max_ald)
    min_dis = min(min_dis, abs(a[i][0]-a[j][0]))
    if a[i][0] > a[j][0]:
        i+=1
    else:
        j-=1

print(min_dis, max_ald)
        
