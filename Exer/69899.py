n,k= [int(x) for x in input().split()]
s = []
for i in range(1, 1+n):
    s.extend([i,i])

li = 0
while len(set(s)) != 1:
    r = ""
    for i in range(k):
        r += str(s[li%len(s)]) + " "
        li += 1
    li = (li-1)%len(s)
    print(r[:-1])
    s.pop(li%len(s))
        
print("winner:"+str(s[0]))
