k = int(input())
s = [int(x) for x in input().split()]
c = 0
for i in range(1, 1001):
    if sum([i%x == 0 for x in s]) == k:
        c+=1 

print(c)
