n = int(input())
k = [int(x) for x in input().split()]
s = [[[False, 0] for __ in range(n)] for _ in range(n)]
        
for i in range(n):
    s[k[i]-1][i][0] = True
def traverse(i,j):
    while i < n:
        while j<n:
            c = s[i][j][1]
            ways = []
            if j <n-1:
                x = s[i][j+1][1]
                p = x and min(x, c+1) or c+1
                s[i][j+1][1] = p
                ways.append((p, (i,j+1)))

            if i<n-1:
                x = s[i+1][j][1]
                p = x and min(x, c+1) or c+1
                s[i+1][j][1] = p
                ways.append((p, (i+1,j)))
            if j <n-1 and i <n-1 and s[i+1][j+1][0]:
                x = s[i+1][j+1][1]
                p = x and min(x, c+1) or c+1
                s[i+1][j+1][1] = p
                ways.append((p, (i+1,j+1)))
            if (i,j) == (n-1,n-1):
                print(s[n-1][n-1][1])
                exit()
            ways.sort(key=lambda x: x[0])
            for w in ways:
                traverse(*w[1])

traverse(0,0)

print(*s, sep="\n")
